import { ADD_CHARACTER, REMOVE_CHARACTER } from "../actions/action-types";

const defaultState = localStorage.getItem("dados salvos")
  ? JSON.parse(window.localStorage.getItem("dados salvos"))
  : [];

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_CHARACTER:
      return [...state, action.character];
    case REMOVE_CHARACTER:
      return state.filter((x) => x.name !== action.name);

    default:
      return state;
  }
};

export default reducer;
