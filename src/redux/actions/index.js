import { ADD_CHARACTER, REMOVE_CHARACTER } from "./action-types";

export const addCollection = (character) => ({
  type: ADD_CHARACTER,
  character: character,
});

export const removeCard = (name) => ({
  type: REMOVE_CHARACTER,
  name: name,
});
