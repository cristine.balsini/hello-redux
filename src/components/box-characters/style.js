import styled from "styled-components";
import "antd/dist/antd.css";
import { Card } from "antd";

const { Meta } = Card;
const DivBox = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const Cards = styled(Card)`
  background-color: orange;
  border: 0;
`;

const Image = styled.img`
  width: 280px;
  height: 220px;
`;

const SubTitle = styled(Meta)`
  font-family: courier;
  font-size: 10px;
`;

export { DivBox, Cards, Image, SubTitle };
