import styled from "styled-components";
import {Link} from "react-router-dom";

const Content = styled.div`
  position: absolute;
  text-align: center;
  min-height: 100vh;
  min-width: 100vh;
  display: flex;
  font-size: calc(10px + 2vmin);
`;

const Header = styled.header`
  display: flex;
  justify-content: space-evenly;
  color: #f7fafc;
  min-width: 100vh;
  background-color: #171923;
`;

const Links = styled(Link)`
  display: flex;
  flex-direction: column-reverse;
  justify-content: space-evenly;
  align-itens: baseline;
  text-align: center;
  font-size: 1.5rem;
  padding: 0.2rem;
  color: whitesmoke;
  height: 10rem;
`;

const ChartDiv = styled.div`
  display: flex;
  background-color: 0;
  margin-top: 8rem;
  min-height: 100vh;

`;

export { Content, Header, Links, ChartDiv };
