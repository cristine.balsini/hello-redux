import React from "react";
import { Pie } from "react-chartjs-2";
import { useSelector } from "react-redux";

const Chart = () => {
  const character = useSelector((state) => state.characters);
  const countPoke = character.filter(({ type }) => {
    if (type === "Pokemon") {
      return {
        type: type,
      };
    }
  });

  const countRick = character.filter(({ type }) => {
    if (type === "Rick and Morty") {
      return {
        type: type,
      };
    }
  });
  const data = {
    labels: ["Pokemon", "Rick and Morty"],
    datasets: [
      {
        data: [countPoke.length, countRick.length],
        backgroundColor: ["#fEA000", "#FF6B0D"],
        hoverBackgroundColor: ["#FEA500", "#E8810C"],
      },
    ],
  };

  return (
    <>
      <Pie data={data} />
    </>
  );
};

export default Chart;
