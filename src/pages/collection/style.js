import styled from "styled-components";
import { Button } from "antd";

const CharacterDiv = styled.div`
  display: flex;
  justify-content: space-around;
  margin-left: 1rem;
  padding: 0.5rem;
`;

const TheButton = styled(Button)`
  margin-top: 2rem;
  font-family: courier;
  color: #171923;
`;

const CollectionDiv = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: center;
`;

export { CharacterDiv, TheButton, CollectionDiv };
