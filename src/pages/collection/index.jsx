import React, { useEffect } from "react";
import Box from "../../components/box-characters";
import { CharacterDiv, TheButton, CollectionDiv } from "./style.js";
import { removeCard } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";

const Collection = ({ switcher, setSwitcher }) => {
  const character = useSelector((state) => state.characters);

  const dispatch = useDispatch();
  const handleCard = ({ name }) => {
    dispatch(removeCard(name));
  };

  const rickCollection = character.filter(({ name, type, image }) => {
    if (type === "Rick and Morty") {
      return {
        name: name,
        type: type,
        imagem: image,
      };
    }
  });

  const pokeCollection = character.filter(({ name, type, image }) => {
    if (type === "Pokemon") {
      return {
        name: name,
        type: type,
        imagem: image,
      };
    }
  });

  return (
    <CollectionDiv>
      <CharacterDiv>
        <TheButton
          shape="round"
          size="large"
          onClick={() => {
            switcher && setSwitcher(false);
            !switcher && setSwitcher(true);
          }}
        >
          {switcher ? "Pokemon" : "Rick's"}
        </TheButton>
      </CharacterDiv>
      {switcher === null && (
        <>
          {character !== undefined && (
            <Box characterList={character} selectCard={handleCard} />
          )}
        </>
      )}

      {switcher === true && <Box characterList={rickCollection} />}
      {switcher === false && <Box characterList={pokeCollection} />}
    </CollectionDiv>
  );
};

export default Collection;
