import styled from "styled-components";
import { Link } from "react-router-dom";
import { BsBoxArrowRight, BsBoxArrowLeft } from "react-icons/bs";

const Pages = styled(Link)`
  padding: 5px;
  font-size: 1rem;
  color: #171923;
  display: flex;
  justify-content: center;
`;

const CharacterDiv = styled.div`
  display: flex;
  color: #171923;
  margin-left: 1rem;
  height: 6rem;
`;

const Right = styled(BsBoxArrowRight)`
  margin-left: 4rem;
`;

const Left = styled(BsBoxArrowLeft)`
  margin-right: 1rem;
`;

export { CharacterDiv, Pages, Right, Left };
