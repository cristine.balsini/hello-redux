import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Box from "../../../components/box-characters";
import { useSelector, useDispatch } from "react-redux";
import { addCollection } from "../../../redux/actions";

const RickMortyCharacters = () => {
  const character = useSelector((state) => state.characters);
  const dispatch = useDispatch();

  const [charactersRick, setCharactersRick] = useState([]);
  const { page } = useParams();

  const handleCard = (x) => {
    character.findIndex((e) => e.name === x.name) === -1 &&
      dispatch(addCollection(x));
  };
  console.log(character, "RICK MORTY");
  const getAPI = () => {
    fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
      .then((resp) => resp.json())
      .then((resp) => {
        const rickMorty = resp.results.map((item) => {
          return {
            name: item.name,
            image: item.image,
            type: "Rick and Morty",
          };
        });

        setCharactersRick(rickMorty);
      });
  };
  useEffect(getAPI, [page]);
  useEffect(() => {
    window.localStorage.setItem("dados salvos", JSON.stringify(character));
  }, [character]);

  return <Box characterList={charactersRick} selectCard={handleCard} />;
};

export default RickMortyCharacters;
