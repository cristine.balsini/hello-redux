import React, { useState, useEffect } from "react";
import Box from "../../../components/box-characters";
import { useSelector, useDispatch } from "react-redux";
import { addCollection } from "../../../redux/actions";

const PokemonCharacters = ({ offset, setOffset }) => {
  const [charactersPokemon, setCharactersPokemon] = useState([]);
  const character = useSelector((state) => state.characters);
  const dispatch = useDispatch();

  const handleCard = (x) => {
    let findMatch = character.some(({ name }) => name === x.name);
    if (findMatch) {
      return;
    }
    dispatch(addCollection(x));
  };

  const getAPI = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`)
      .then((resp) => resp.json())
      .then((resp) => {
        const result = resp.results.map((item) => {
          const url = item.url.split("/");

          return {
            name: item.name,
            image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
              url[url.length - 2]
            }.png`,
            type: "Pokemon",
          };
        });
        setCharactersPokemon(result);
      });
  };

  useEffect(getAPI, [offset]);
  useEffect(() => {
    window.localStorage.setItem("dados salvos", JSON.stringify(character));
  }, [character]);

  return <Box characterList={charactersPokemon} selectCard={handleCard} />;
};

export default PokemonCharacters;
