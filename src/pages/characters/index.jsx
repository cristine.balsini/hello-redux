import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { motion } from "framer-motion";
import { Button } from "antd";
import PokemonCharacters from "./pokemon";
import RickMortyCharacters from "./rick-and-morty";
import { CharacterDiv, Pages, Right, Left } from "./style.js";

const Characters = () => {
  const [changeList, setChangeList] = useState(true);
  const { page } = useParams();
  const history = useHistory();
  const [offset, setOffset] = useState(0);

  const nextOffset = () => {
    offset > 159 ? setOffset(160) : setOffset(offset + 20);
  };
  const previousOffset = () => {
    offset < 1 ? setOffset(0) : setOffset(offset - 20);
  };

  return (
    <div>
      {changeList &&
        (page < 2 ? (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={"/characters/1"}>
              <Left />
            </Pages>
          </motion.div>
        ) : (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={`${page - 1}`}>
              <Left />
            </Pages>
          </motion.div>
        ))}
      {!changeList &&
        (page < 2 ? (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={"/characters/1"}>
              <Left />
            </Pages>
          </motion.div>
        ) : (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages onClick={previousOffset} to={`/characters/${page - 1}`}>
              <Left />
            </Pages>
          </motion.div>
        ))}
      {changeList &&
        (page > 33 ? (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={"/characters/34"}>
              <Right />
            </Pages>
          </motion.div>
        ) : (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={`${parseInt(page) + 1}`}>
              <Right />
            </Pages>
          </motion.div>
        ))}

      {!changeList &&
        (page > 8 ? (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages to={"/characters/9"}>
              <Right />
            </Pages>
          </motion.div>
        ) : (
          <motion.div whileHover={{ scale: 2 }} whileTap={{ scale: 0.8 }}>
            <Pages
              onClick={nextOffset}
              to={`/characters/${parseInt(page) + 1}`}
            >
              <Right />
            </Pages>
          </motion.div>
        ))}
      <CharacterDiv>
        <Button
          shape="round"
          size="large"
          style={{
            position: "relative",
            color: "#171923",
            fontFamily: "courier",
          }}
          onClick={() => {
            setChangeList(!changeList);
            history.push("/characters/1");
            setOffset(0);
          }}
        >
          {changeList ? "Get Pokemon!" : "Rick and Morty"}
        </Button>
      </CharacterDiv>
      {changeList ? (
        <RickMortyCharacters />
      ) : (
        <PokemonCharacters offset={offset} setOffset={setOffset} />
      )}
    </div>
  );
};

export default Characters;
